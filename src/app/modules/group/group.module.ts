import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupRoutingModule } from './group-routing.module';
import { GroupComponent } from './pages/index/group.component';
import { MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatSortModule, MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatDialogModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatOptionModule,
  MatSelectModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { GroupService } from './group.service';
import { DeleteDialogComponent } from './dialogs/delete/delete.dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SaveDialogComponent } from './dialogs/save/save.dialog.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GroupDetailComponent } from './pages/detail/group.component';


@NgModule({
  declarations: [GroupComponent,
    GroupDetailComponent,
    SaveDialogComponent,
    DeleteDialogComponent],
  imports: [
    CommonModule,
    GroupRoutingModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatOptionModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule
  ],
  entryComponents: [
    SaveDialogComponent,
    DeleteDialogComponent
  ],
  providers: [GroupService]
})
export class GroupModule { }
