import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupComponent } from './pages/index/group.component';
import { GroupDetailComponent } from './pages/detail/group.component';


const routes: Routes = [
  {
    path: '',
    component: GroupComponent
  },
  {
    path: ':id',
    component: GroupDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupRoutingModule { }
