import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Group } from '../../group.model';
import { GroupService } from '../../group.service';

@Component({
  selector: 'app-save.dialog',
  templateUrl: './save.dialog.html',
  styleUrls: ['./save.dialog.css']
})

export class SaveDialogComponent {


  selected: number;
  groups: Group[] = [];
  loading = false;


  constructor(public dialogRef: MatDialogRef<SaveDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public group: Group,
              public groupService: GroupService) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onChange(value: number): void {
    console.log(this.group);
    console.log(value);
    const group = this.groups.find(grupo => grupo.id === value);
    console.log(group);
    this.group = group;
    console.log(this.group);
  }

  public confirmAdd(): void {
    console.log(this.group);
    this.loading = true;
    if (this.group.id) {
      console.log('update');
      this.groupService.update(this.group).subscribe(
          group => this.dialogRef.close(group),
          () => this.loading = false
        );
    } else {
    console.log('create');
    this.groupService.add(this.group).subscribe(
        group => this.dialogRef.close(group),
        () => this.loading = false
      );
    }
  }
}
