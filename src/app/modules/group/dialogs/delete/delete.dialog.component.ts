import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import { Group } from '../../group.model';
import { GroupService } from '../../group.service';


@Component({
  selector: 'app-delete.dialog',
  templateUrl: '../../dialogs/delete/delete.dialog.html',
  styleUrls: ['../../dialogs/delete/delete.dialog.css']
})
export class DeleteDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Group, public groupService: GroupService) {
                console.log(data);
              }

  loading = false;

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.loading = true;

    this.groupService.delete(this.data.id).subscribe(
      response => (this.dialogRef.close(response)),
      () => this.loading = false
    );
  }
}
