import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Group } from './group.model';
import { Page } from '../../shared/model/page';
import { HttpClient, HttpParams } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from 'src/app/app-config.module';
import { PaginationRequest } from 'src/app/shared/model/pagination-request';
import { MyCustomHttpUrlEncodingCodec } from 'src/app/shared/utils/mycustomhttpurlencodingcodec';

@Injectable()
export class GroupService {

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
    ) { }

  getAll(pr: PaginationRequest = new PaginationRequest()): Observable<Page<Group>> {

    const page = pr.page || '1';
    const limit = pr.limit || '20';
    const sort = pr.sort || 'updated,DESC';
    const filter = pr.filter ? `${pr.filter.key}||${pr.filter.criteria}||${pr.filter.value}` : '';

    console.log(pr);

    let params = new HttpParams({encoder: new MyCustomHttpUrlEncodingCodec()})
      .set('page', page)
      .set('limit', limit)
      .set('sort', sort)
      .set('filter', filter);

    pr.join.forEach(data => {
      params = params.append('join[]', data);
    });

    return this.http.get<Page<Group>>(`${this.config.apiEndpoint}groups`, {params});
  }

  findById(id: number): Observable<Group> {
    return this.http.get<Group>(`${this.config.apiEndpoint}groups/${id}`);
  }

  add(group: Group): Observable<Group> {
    console.log('posting...');
    return this.http.post<Group>(`${this.config.apiEndpoint}groups`, group);
  }

  update(group: Group): Observable<Group> {
    return this.http.put<Group>(`${this.config.apiEndpoint}groups/${group.id}`, group);
  }

  delete(id: number) {
    return this.http.delete(`${this.config.apiEndpoint}groups/${id}`);
  }
}
