export class Group {
  id: number;
  name: string;
  description: string;
  created: Date;
  updated: Date;
}
