import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable, BehaviorSubject, of} from 'rxjs';
import {catchError, finalize, take, first} from 'rxjs/operators';
import { Group } from '../../group.model';
import { Page } from '../../../../shared/model/page';
import { GroupService } from '../../group.service';
import { PaginationRequest } from 'src/app/shared/model/pagination-request';



export class GroupDataSource implements DataSource<Group> {

    private pageSubject = new BehaviorSubject<Page<Group>>(new Page<Group>());

    private groupsSubject = new BehaviorSubject<Group[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    public page$ = this.pageSubject.asObservable();

    constructor(private groupService: GroupService) {

    }

    /**
     * Gets data from API according PaginationRequest, otherwise will add or remove an element
     * on the front end, after adding o removing
     */
    getData(pr: PaginationRequest = new PaginationRequest(), group?: Group) {
        // no need show loading spinner since is almost immediatly
        if (group) {
           this.groupsSubject.pipe(first()).subscribe((groups: Group[]) => {
                   console.log(group);
                   groups.pop();
                   this.groupsSubject.next([group, ...groups]);
                   console.log(this.groupsSubject.getValue());

                   this.pageSubject.pipe(first()).subscribe((page: Page<Group>) => {
                    console.log(page);
                    page.total++;
                    this.pageSubject.next(page);
                    console.log(this.pageSubject.getValue());
                   });
                }
           );

        } else {
            this.loadingSubject.next(true);
            this.groupService.getAll(pr).pipe(
                    catchError(() => of([])),
                    finalize(() => this.loadingSubject.next(false))
                )
                .subscribe((page: Page<Group>) => {
                    this.groupsSubject.next(page.data);
                    this.pageSubject.next(page);
                });
        }

    }
    delete(group: Group): number {
        this.groupsSubject.pipe(first()).subscribe((groups: Group[]) => {
            console.log(groups);
            groups.splice(groups.findIndex(rgroup => rgroup.id === group.id), 1);
            this.groupsSubject.next(groups);
            console.log(this.groupsSubject.getValue());

            this.pageSubject.pipe(first()).subscribe((page: Page<Group>) => {
             console.log(page);
             page.total--;
             this.pageSubject.next(page);
             console.log(this.pageSubject.getValue());
            });
         }
        );
        return this.groupsSubject.getValue().length;
    }

    connect(): Observable<Group[]> {
        console.log('Connecting data source');
        return this.groupsSubject.asObservable();
    }

    disconnect(): void {
        this.groupsSubject.complete();
        this.loadingSubject.complete();
        this.pageSubject.complete();
    }

}

