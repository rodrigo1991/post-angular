import { AfterViewInit, Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { GroupDataSource } from './group-datasource';
import { GroupService } from '../../group.service';
import { Group } from '../../group.model';
import { tap, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { merge, fromEvent, Observable } from 'rxjs';
import { PaginationRequest } from 'src/app/shared/model/pagination-request';
import { SaveDialogComponent } from '../../dialogs/save/save.dialog.component';
import { DeleteDialogComponent } from '../../dialogs/delete/delete.dialog.component';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupDetailComponent implements AfterViewInit, OnInit {
  group$: Observable<Group>;

  @ViewChild(MatPaginator, {static: false})
  private paginator: MatPaginator;

  @ViewChild(MatSort, {static: false})
  private sort: MatSort;

  @ViewChild(MatTable, {static: false})
  private table: MatTable<Group>;

  @ViewChild('fitler', { static: false })
  fitler: ElementRef;

  dataSource: GroupDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['group', 'name', 'surname', 'updated', 'actions'];

  private pr: PaginationRequest;

  constructor(private readonly groupService: GroupService,
              public dialog: MatDialog,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.group$ = this.groupService.findById(id);
    this.dataSource = new GroupDataSource(this.groupService, id);
    this.refresh();
  }

  ngAfterViewInit() {

    fromEvent(this.fitler.nativeElement, 'keyup')
            .pipe(
                debounceTime(150),
                distinctUntilChanged(),
                tap(() => {

                  this.pr.filter = this.fitler.nativeElement.value ? {
                    key: 'name',
                    criteria: 'cont',
                    value: this.fitler.nativeElement.value
                  } : undefined;

                  this.dataSource.getData(this.pr);
                })
            )
            .subscribe();

    merge(this.sort.sortChange, this.paginator.page)
        .pipe(
            tap(() => {
              console.log('paginando');
              this.pr.sort = '';
              if (this.sort.active && this.sort.direction !== '') {
                this.pr.sort = `${this.sort.active},${this.sort.direction.toUpperCase()}`;
              }
              this.pr.limit = this.paginator.pageSize.toString();
              this.pr.page = (this.paginator.pageIndex + 1).toString();
              this.dataSource.getData(this.pr);
            })
        )
        .subscribe();

    this.table.dataSource = this.dataSource;
  }

  private refresh() {
    this.pr = new PaginationRequest();
    this.pr.join.add('group');
    this.dataSource.getData(this.pr);
    this.dataSource.getData(this.pr);
  }

  saveDialog(group: Group = new Group()) {
    const dialogRef = this.dialog.open(SaveDialogComponent, {
      data: group
    });

    dialogRef.afterClosed().subscribe(savedGroup => {
      console.log(savedGroup);
      if (savedGroup) {
        if (!group.id) {// only inserts
          this.dataSource.getData(null, savedGroup);
        }
      }
    });
  }

  delete(group: Group) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: group
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result === null) {
        // const foundIndex = this.exampleDatabase.dataChange.value.findIndex(x => x.id === this.id);
        // for delete we use splice in order to remove single object from DataService
        // this.exampleDatabase.dataChange.value.splice(foundIndex, 1);
        // this.refreshTable();
        const groupsToShow: number = this.dataSource.delete(group);
        if (groupsToShow) {
          this.paginator.pageSize--;
        } else {
          this.refresh();
          this.paginator.pageSize = 20;
        }
      }
    });
  }
}
