import { Group } from '../group/group.model';

export class User {
  id: number;
  group: Group;
  name: string;
  surname: string;
  birthdate: Date;
  created: Date;
  updated: Date;

  constructor() {
    this.group = new Group();
  }
}
