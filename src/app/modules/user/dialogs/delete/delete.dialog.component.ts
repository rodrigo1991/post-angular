import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import { UserService } from '../../user.service';
import { User } from '../../user.model';


@Component({
  selector: 'app-delete.dialog',
  templateUrl: '../../dialogs/delete/delete.dialog.html',
  styleUrls: ['../../dialogs/delete/delete.dialog.css']
})
export class DeleteDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: User, public userService: UserService) {
                console.log(data);
              }

  loading = false;

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.loading = true;

    this.userService.delete(this.data.id).subscribe(
      response => (this.dialogRef.close(response)),
      () => this.loading = false
    );
  }
}
