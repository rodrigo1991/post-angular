import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { User } from '../../user.model';
import { UserService } from '../../user.service';
import { Group } from '../../../group/group.model';

@Component({
  selector: 'app-save.dialog',
  templateUrl: './save.dialog.html',
  styleUrls: ['./save.dialog.css']
})

export class SaveDialogComponent {


  selected: number;
  groups: Group[] = [];
  loading = false;


  constructor(public dialogRef: MatDialogRef<SaveDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public user: User,
              public userService: UserService) {
                console.log(user);

                const group1 = new Group();
                group1.id = 1;
                group1.name = 'Admin';

                const group2 = new Group();
                group2.id = 2;
                group2.name = 'Users';

                this.groups.push(group1);
                this.groups.push(group2);

                this.selected = this.user.group.id;
              }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onChange(value: number): void {
    console.log(this.user);
    console.log(value);
    const group = this.groups.find(grupo => grupo.id === value);
    console.log(group);
    this.user.group = group;
    console.log(this.user);
  }

  public confirmAdd(): void {
    console.log(this.user);
    this.loading = true;
    if (this.user.id) {
      console.log('update');
      this.userService.update(this.user).subscribe(
          user => this.dialogRef.close(user),
          () => this.loading = false
        );
    } else {
    console.log('create');
    this.userService.add(this.user).subscribe(
        user => this.dialogRef.close(user),
        () => this.loading = false
      );
    }
  }
}
