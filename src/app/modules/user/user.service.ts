import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from './user.model';
import { Page } from '../../shared/model/page';
import { HttpClient, HttpParams } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from 'src/app/app-config.module';
import { PaginationRequest } from 'src/app/shared/model/pagination-request';
import { MyCustomHttpUrlEncodingCodec } from 'src/app/shared/utils/mycustomhttpurlencodingcodec';

@Injectable()
export class UserService {

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
    ) { }

  getAll(pr: PaginationRequest = new PaginationRequest()): Observable<Page<User>> {

    const page = pr.page || '1';
    const limit = pr.limit || '20';
    const sort = pr.sort || 'updated,DESC';
    const filter = pr.filter ? `${pr.filter.key}||${pr.filter.criteria}||${pr.filter.value}` : '';

    console.log(pr);

    let params = new HttpParams({encoder: new MyCustomHttpUrlEncodingCodec()})
      .set('page', page)
      .set('limit', limit)
      .set('sort', sort)
      .set('filter', filter);

    pr.join.forEach(data => {
      params = params.append('join[]', data);
    });

    return this.http.get<Page<User>>(`${this.config.apiEndpoint}users`, {params});
  }

  findById(id: number): Observable<User> {
    return this.http.get<User>(`${this.config.apiEndpoint}users/${id}`);
  }

  add(user: User): Observable<User> {
    console.log('posting...');
    return this.http.post<User>(`${this.config.apiEndpoint}groups/${user.group.id}/users`, user);
  }

  update(user: User): Observable<User> {
    return this.http.put<User>(`${this.config.apiEndpoint}groups/${user.group.id}/users/${user.id}`, user);
  }

  delete(id: number) {
    return this.http.delete(`${this.config.apiEndpoint}users/${id}`);
  }
}
