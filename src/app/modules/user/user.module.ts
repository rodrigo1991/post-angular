import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './pages/index/user.component';
import { MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatSortModule, MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatDialogModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatOptionModule,
  MatSelectModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';
import { DeleteDialogComponent } from './dialogs/delete/delete.dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SaveDialogComponent } from './dialogs/save/save.dialog.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserDetailComponent } from './pages/detail/user.component';


@NgModule({
  declarations: [UserComponent,
    UserDetailComponent,
    SaveDialogComponent,
    DeleteDialogComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatOptionModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule
  ],
  entryComponents: [
    SaveDialogComponent,
    DeleteDialogComponent
  ],
  providers: [UserService]
})
export class UserModule { }
