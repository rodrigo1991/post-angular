import { AfterViewInit, Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { UserDataSource } from './user-datasource';
import { UserService } from '../../user.service';
import { User } from '../../user.model';
import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { merge, fromEvent } from 'rxjs';
import { PaginationRequest } from 'src/app/shared/model/pagination-request';
import { SaveDialogComponent } from '../../dialogs/save/save.dialog.component';
import { DeleteDialogComponent } from '../../dialogs/delete/delete.dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false})
  private paginator: MatPaginator;

  @ViewChild(MatSort, {static: false})
  private sort: MatSort;

  @ViewChild(MatTable, {static: false})
  private table: MatTable<User>;

  @ViewChild('fitler', { static: false })
  fitler: ElementRef;

  dataSource: UserDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['group', 'name', 'surname', 'updated', 'actions'];

  private pr: PaginationRequest;

  constructor(private readonly userService: UserService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource = new UserDataSource(this.userService);
    this.refresh();
  }

  ngAfterViewInit() {

    fromEvent(this.fitler.nativeElement, 'keyup')
            .pipe(
                debounceTime(150),
                distinctUntilChanged(),
                tap(() => {

                  this.pr.filter = this.fitler.nativeElement.value ? {
                    key: 'name',
                    criteria: 'cont',
                    value: this.fitler.nativeElement.value
                  } : undefined;

                  this.dataSource.getData(this.pr);
                })
            )
            .subscribe();

    merge(this.sort.sortChange, this.paginator.page)
        .pipe(
            tap(() => {
              console.log('paginando');
              this.pr.sort = '';
              if (this.sort.active && this.sort.direction !== '') {
                this.pr.sort = `${this.sort.active},${this.sort.direction.toUpperCase()}`;
              }
              this.pr.limit = this.paginator.pageSize.toString();
              this.pr.page = (this.paginator.pageIndex + 1).toString();
              this.dataSource.getData(this.pr);
            })
        )
        .subscribe();

    this.table.dataSource = this.dataSource;
  }

  private refresh() {
    this.pr = new PaginationRequest();
    this.pr.join.add('group');
    this.dataSource.getData(this.pr);
    this.dataSource.getData(this.pr);
  }

  saveDialog(user: User = new User()) {
    const dialogRef = this.dialog.open(SaveDialogComponent, {
      data: user
    });

    dialogRef.afterClosed().subscribe(savedUser => {
      console.log(savedUser);
      if (savedUser) {
        if (!user.id) {// only inserts
          this.dataSource.getData(null, savedUser);
        }
      }
    });
  }

  delete(user: User) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: user
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result === null) {
        // const foundIndex = this.exampleDatabase.dataChange.value.findIndex(x => x.id === this.id);
        // for delete we use splice in order to remove single object from DataService
        // this.exampleDatabase.dataChange.value.splice(foundIndex, 1);
        // this.refreshTable();
        const usersToShow: number = this.dataSource.delete(user);
        if (usersToShow) {
          this.paginator.pageSize--;
        } else {
          this.refresh();
          this.paginator.pageSize = 20;
        }
      }
    });
  }
}
