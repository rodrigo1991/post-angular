import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable, BehaviorSubject, of} from 'rxjs';
import {catchError, finalize, take, first} from 'rxjs/operators';
import { User } from '../../user.model';
import { Page } from '../../../../shared/model/page';
import { UserService } from '../../user.service';
import { PaginationRequest } from 'src/app/shared/model/pagination-request';



export class UserDataSource implements DataSource<User> {

    private pageSubject = new BehaviorSubject<Page<User>>(new Page<User>());

    private usersSubject = new BehaviorSubject<User[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    public page$ = this.pageSubject.asObservable();

    constructor(private userService: UserService, private id: number) {

    }

    /**
     * Gets data from API according PaginationRequest, otherwise will add or remove an element
     * on the front end, after adding o removing
     */
    getData(pr: PaginationRequest = new PaginationRequest(), user?: User) {
        // no need show loading spinner since is almost immediatly
        if (user) {
           this.usersSubject.pipe(first()).subscribe((users: User[]) => {
                   console.log(user);
                   users.pop();
                   this.usersSubject.next([user, ...users]);
                   console.log(this.usersSubject.getValue());

                   this.pageSubject.pipe(first()).subscribe((page: Page<User>) => {
                    console.log(page);
                    page.total++;
                    this.pageSubject.next(page);
                    console.log(this.pageSubject.getValue());
                   });
                }
           );

        } else {
            this.loadingSubject.next(true);
            this.userService.getAll(pr).pipe(
                    catchError(() => of([])),
                    finalize(() => this.loadingSubject.next(false))
                )
                .subscribe((page: Page<User>) => {
                    this.usersSubject.next(page.data);
                    this.pageSubject.next(page);
                });
        }

    }
    delete(user: User): number {
        this.usersSubject.pipe(first()).subscribe((users: User[]) => {
            console.log(users);
            users.splice(users.findIndex(ruser => ruser.id === user.id), 1);
            this.usersSubject.next(users);
            console.log(this.usersSubject.getValue());

            this.pageSubject.pipe(first()).subscribe((page: Page<User>) => {
             console.log(page);
             page.total--;
             this.pageSubject.next(page);
             console.log(this.pageSubject.getValue());
            });
         }
        );
        return this.usersSubject.getValue().length;
    }

    connect(): Observable<User[]> {
        console.log('Connecting data source');
        return this.usersSubject.asObservable();
    }

    disconnect(): void {
        this.usersSubject.complete();
        this.loadingSubject.complete();
        this.pageSubject.complete();
    }

}

