import { Injectable } from '@angular/core';

@Injectable()
export class PaginationRequest {
  page: string;
  limit: string;
  sort: string;
  filter: {
    key: string,
    criteria: string,
    value: string
  };
  join: Set<string>;

  constructor() {
    this.join = new Set();
  }
}
