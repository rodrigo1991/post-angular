import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainNavComponent } from './main-nav.component';


const routes: Routes = [
  {
    path: '',
    component: MainNavComponent,
    children: [
      {
        path: 'users',
        loadChildren: () => import('../../modules/user/user.module').then(mod => mod.UserModule)
      },
      {
        path: 'groups',
        loadChildren: () => import('../../modules/group/group.module').then(mod => mod.GroupModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('../../modules/dash/dash.module').then(mod => mod.DashModule)
      },
      {
        path: 'drag',
        loadChildren: () => import('../../modules/drag/drag.module').then(mod => mod.DragModule)
      },
      {
        path: 'add-form',
        loadChildren: () => import('../../modules/add-form/add-form.module').then(mod => mod.AddFormModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainNavRoutingModule { }
